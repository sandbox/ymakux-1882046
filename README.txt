The Userpoints Bulk Edit module provides an action for Views Bulk Operations (a.k.a VBO) module
that allows you to add/subtract userpoints for several users at once.

Installation:
=============
Download and enable at admin/modules. Nothing special.

Usage:
======
Upon installation, this module adds a new view,
which can be used to bulk manage user points "out-of-box".
Go to admin/config/people/userpoints/bulk for that.

You can also create any custom view (User entity type),
add Views Bulk Operations field and select 
"Userpoints: Add/subtract (vbo_userpoints_add_userpoints_action)"
under "Selected bulk operations" fieldset

Author:
=======
Iurii Makukh
ymakux@gmail.com