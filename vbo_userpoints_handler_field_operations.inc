<?php

/**
* @file
* Views field handler. Implements the Views Form API
*/
class vbo_userpoints_handler_field_operations extends views_bulk_operations_handler_field_operations {

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $action = 'action::vbo_userpoints_add_userpoints_action';
    $class = 'edit-options-vbo-operations-actionvbo-userpoints-add-userpoints-action-selected';
    $form_structure = vbo_userpoints_add_userpoints_form_structure();

    $options = $this->options['vbo_operations'][$action]['default'];

    $form['vbo_operations'][$action]['default'] = array(
      '#title' => t('Default values'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#dependency' => array(
        $class => array(1),
      ),
    );

    foreach ($form_structure as $key => $value) {
      $form['vbo_operations'][$action]['default'][$key] = $value;
      $form['vbo_operations'][$action]['default'][$key]['#default_value'] = isset($options[$key]) ? $options[$key] : NULL;
    }
  }
}
